<?php

namespace Ilin\ElasticsearchQueryBuilder\Queries;

interface Query
{
    public function toArray(): array;
}
