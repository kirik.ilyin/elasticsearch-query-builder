<?php

namespace Ilin\ElasticsearchQueryBuilder\Aggregations\Concerns;

use Ilin\ElasticsearchQueryBuilder\AggregationCollection;
use Ilin\ElasticsearchQueryBuilder\Aggregations\Aggregation;

trait WithAggregations
{
    protected AggregationCollection $aggregations;

    public function aggregation(Aggregation $aggregation): self
    {
        $this->aggregations->add($aggregation);

        return $this;
    }
}
